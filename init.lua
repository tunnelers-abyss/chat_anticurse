-- Minetest 0.4.10+ mod: chat_anticurse
-- punish player for cursing by disconnecting them
--
--  Created in 2015 by Andrey.
--  This mod is Free and Open Source Software, released under the LGPL 2.1 or later.
--
-- See README.txt for more information.

chat_anticurse = {}
chat_anticurse.simplemask = {}
chat_anticurse.cursemask = {}
chat_anticurse.shadow_muted = {}

-- some english and some russian curse words
-- i don't want to keep these words as cleartext in code, so they are stored like this.
local x1="a+"
local x2="[i1!|]+"
local x3="u+"
local x4="[e3]+"
local x5="[o0о@]+"
local y1="y+"
local y2="и+"
local y3="о+"
local y4="е+"
local y5="я+"
local z1="[^A-Za-z0-9]*"
local z2="[%(%[<{]+[%)%]>}]+"

chat_anticurse.simplemask[1] = " "..x1.."s" .. "s "
chat_anticurse.simplemask[2] = " d" ..z1.. ""..x2..z1.."c"..z1.."k "
chat_anticurse.simplemask[3] = " p"..z1..x4..z1.."n" ..z1.. "i"..z1.."s"
chat_anticurse.simplemask[4] = " p" ..z1.. ""..x3..z1.."s"..z1.."s"..z1.."y"
chat_anticurse.simplemask[5] = " h"..x5.."" .. "r"..z1.."n"..z1.."y "
chat_anticurse.simplemask[6] = " b"..z1..x2..z1.."" .. "t"..z1.."c"..z1.."h "
chat_anticurse.simplemask[7] = " b"..z1..x2..z1.."" .. "t"..z1.."c"..z1.."h"..z1.."e"
chat_anticurse.simplemask[8] = " s"..x4.."" .. "x"
chat_anticurse.simplemask[9] = " "..y4.."б" .. "а"
chat_anticurse.simplemask[10] = " бл"..y5.."" .. " "
chat_anticurse.simplemask[11] = " ж" .. ""..y3.."п"
chat_anticurse.simplemask[12] = " х" .. ""..y1.."й"
chat_anticurse.simplemask[13] = " ч" .. "л"..y4.."н"
chat_anticurse.simplemask[14] = " п"..y2.."" .. "зд"
chat_anticurse.simplemask[15] = " в"..y3.."" .. "збуд"
chat_anticurse.simplemask[16] = " в"..y3.."з" .. "б"..y1.."ж"
chat_anticurse.simplemask[17] = " сп"..y4.."" .. "рм"
chat_anticurse.simplemask[18] = " бл"..y5.."" .. "д"
chat_anticurse.simplemask[19] = " бл"..y5.."" .. "ть"
chat_anticurse.simplemask[20] = " с" .. ""..y4.."кс"
chat_anticurse.simplemask[21] = " f" .. ""..x3.."ck"
chat_anticurse.simplemask[22] = ""..x1.."rs"..x4.."h"..x5.."l"..x4..""
chat_anticurse.simplemask[23] = " c"..x3.."nt "
chat_anticurse.simplemask[24] = " b"..x5.."l"..x3.."d"..x1
chat_anticurse.simplemask[25] = " b"..x5.."l"..x3.."d"..x5
chat_anticurse.simplemask[26] = " c"..x1.."g"..x1.."r"
chat_anticurse.simplemask[27] = " c".."h"..x2.."n".."g"..x1
chat_anticurse.simplemask[28] = " c".."h"..x3.."c".."h"..x1
chat_anticurse.simplemask[29] = " c"..x5.."g"..x4.."r"
chat_anticurse.simplemask[30] = " c"..x5.."j"..x2.."d"..x5
chat_anticurse.simplemask[31] = " c"..x5.."j"..x5.."n"..x4.."s"
chat_anticurse.simplemask[32] = " c"..x5.."n".."c".."h"..x1
chat_anticurse.simplemask[33] = " c"..x5.."n".."c".."h"..x3.."d"..x5
chat_anticurse.simplemask[34] = " c"..x5.."ñ"..x5
chat_anticurse.simplemask[35] = " c"..x5.."r".."n"..x3.."d"..x5
chat_anticurse.simplemask[36] = " c"..x3.."l"..x4..x1.."d"..x5
chat_anticurse.simplemask[37] = " c"..x3.."l"..x4.."r"..x5
chat_anticurse.simplemask[38] = " j"..x5.."d"..x4.."r"
chat_anticurse.simplemask[39] = " m"..x1.."r"..x2.."c"..x5.."n"
chat_anticurse.simplemask[40] = " m"..x2..x4.."r".."d"..x1
chat_anticurse.simplemask[41] = " p"..x1.."j"..x4.."r"..x1
chat_anticurse.simplemask[42] = " p"..x1.."j"..x4.."r"..x5
chat_anticurse.simplemask[43] = " p"..x4.."l"..x5.."t"..x3.."d"..x1
chat_anticurse.simplemask[44] = " p"..x4.."l"..x5.."t"..x3.."d"..x5
chat_anticurse.simplemask[45] = " p"..x4.."n".."d"..x4.."j"..x5
chat_anticurse.simplemask[46] = " p"..x2.."j"..x1
chat_anticurse.simplemask[47] = " n"..x2.."gg"..z1
chat_anticurse.simplemask[48] = " p"..x2.."n".."g"..x1
chat_anticurse.simplemask[49] = " p"..x2.."t"..x5
chat_anticurse.simplemask[50] = " p"..x5.."l".."l"..x1
chat_anticurse.simplemask[51] = " t"..x1.."r"..x1.."d"..x1
chat_anticurse.simplemask[52] = " t"..x1.."r"..x1.."d"..x5
chat_anticurse.simplemask[53] = " w+"..z1.."t+"..z1.."f+"
chat_anticurse.simplemask[54] = " sh"..x2.."t"
chat_anticurse.simplemask[55] = " m"..x4.."rd"..x4
chat_anticurse.simplemask[56] = " ch"..x2..x4.."r"
chat_anticurse.simplemask[57] = " p"..x3.."t"..x1..x2.."n"
chat_anticurse.simplemask[58] = " b"..x5.."rd"..x4.."l"

chat_anticurse.cursemask[1] = " "..x5..z1.."m"..z1.."f"..z1.."g"
chat_anticurse.cursemask[2] = " g"..z1..x4..z1..""..x4..z1.."z"
chat_anticurse.cursemask[3] = " j"..z1..x4..z1..""..""..x4..z1.."z"
chat_anticurse.cursemask[4] = " g"..x1..z1..""..z1.."w"..z1.."d"
chat_anticurse.cursemask[5] = " "..z2.."h%s+"..z1.."m"..z1.."y+%s+"..z1.."g"..z1.."o"..z1.."d"
chat_anticurse.cursemask[6] = " z"..z2.."m"..z1.."f?"..z1.."".."g"
chat_anticurse.cursemask[7] = " "..x5.."h%s+m"..x5.."n%s+d"..x2..x4..x3

chat_anticurse.check_message = function(name, message)
    local checkingmessage=string.lower( name.." "..message .." " )
    for i=1, #chat_anticurse.simplemask do
        if string.find(checkingmessage, chat_anticurse.simplemask[i]) ~=nil then
            --minetest.chat_send_all(chat_anticurse.simplemask[i])
            return 2
        end
    end

    for i=1, #chat_anticurse.cursemask do
        if string.find(checkingmessage, chat_anticurse.cursemask[i]) ~=nil then
            return 3
        end
    end

    --additional checks
    if
        string.find(checkingmessage, " c"..x3.."" .. "m ") ~=nil and
        not (string.find(checkingmessage, " c"..x3.."" .. "m " .. "se") ~=nil) and
        not (string.find(checkingmessage, " c"..x3.."" .. "m " .. "to") ~=nil)
    then
        return 2
    end
    return 0
end

chat_anticurse.handle_censure = function(uncensored, name, message, way)
    if uncensored == 1 then
        minetest.kick_player(name, "Hey! Was there a bad word?")
        minetest.log("action", "Player "..name.." warned for cursing. "..way..":"..message)
        return true
    end

    if uncensored == 2 then
        minetest.kick_player(name, "Cursing or words, inappropriate to game server. Kids may be playing here!")
        minetest.chat_send_all("Player <"..name.."> warned for cursing" )
        minetest.log("action", "Player "..name.." warned for cursing. "..way..":"..message)
        return true
    end

    if uncensored == 3 then
        minetest.kick_player(name, "Do you know what you said just now? That was not a prayer!")
        minetest.chat_send_all("Player <"..name.."> warned for cursing" )
        minetest.log("action", "Player "..name.." warned for cursing. "..way..":"..message)
        return true
    end

    return false
end

chat_anticurse.goodness = function(str)
	str = str:gsub("([a-zA-Z][oO0@]+)("..z1.."[mM]+%s)","%1＀xxx＀%2")
	str = str:gsub("([oO0@]+)("..z1.."[mM]+)("..z1.."[gG]+)","%1h n%1")
	str = str:gsub("(%(%))("..z1.."[mM]+)("..z1.."[gG]+)","%1h n%1")
	return str:gsub("([a-zA-Z][oO0@]+)＀xxx＀("..z1.."[mM]+%s)","%1%2")
end

minetest.register_on_chat_message(function(name, message)
    local uncensored = chat_anticurse.check_message(name, message)

    local r = chat_anticurse.handle_censure(uncensored, name, message, "Chat")

    if chat_anticurse.shadow_muted[name] then
        -- echo a fake message back
        minetest.chat_send_player(name, minetest.format_chat_message(name, message))
        minetest.log("action", "Player "..name.." shadow muted: message was: "..message)
        return true
    end

    return r
end)

if minetest.chatcommands["me"] then
    local old_command = minetest.chatcommands["me"].func
    minetest.override_chatcommand("me", {func = function(name, param)
        local uncensored = chat_anticurse.check_message(name, param)
        param = chat_anticurse.goodness(param)

	core.log("action", "ME: <" .. name .. "> " .. param)
        if chat_anticurse.handle_censure(uncensored, name, param, "Chat") then
            return true
        end

        if chat_anticurse.shadow_muted[name] then
            -- echo a fake message back
            minetest.chat_send_player(name, "* "..name.." "..param)
            return true
        end

        return old_command(name, param)
    end})
end

if minetest.chatcommands["msg"] then
    local old_command = minetest.chatcommands["msg"].func
    minetest.override_chatcommand("msg", {func = function(name, param)
        if not param or param == "" then return end
        local uncensored = chat_anticurse.check_message(name, param)
        param = chat_anticurse.goodness(param)

        if chat_anticurse.handle_censure(uncensored, name, param, "Chat") then
            return true
        end

        if chat_anticurse.shadow_muted[name] then
            -- echo a fake message back
            local splits = {}
            splits[1], splits[2] = param:match("(%w+)(.+)")

            minetest.chat_send_player(name, "DM sent to "..splits[1]..":"..splits[2])
            minetest.chat_send_player(name, "Message sent.")
            return true
        else
            local splits = {}
            splits[1], splits[2] = param:match("(%w+)(.+)")

            minetest.chat_send_player(name, "DM sent to "..splits[1]..":"..splits[2])
        end

        return old_command(name, param)
    end})
end

-- Hyper mega
minetest.register_on_prejoinplayer(function(name)
    local checkingmessage = string.lower(" ".. name)
    checkingmessage = checkingmessage:gsub("_", " ")
    for i=1, #chat_anticurse.simplemask do
        if string.find(checkingmessage, chat_anticurse.simplemask[i]) and minetest.player_exists(name) == false then
            -- Hyper
            minetest.log("The user: " .. name .. " tried to join with a bad name.")
            return "Your name contains a curse word which isn't allowed here. Please use a different nickname."
        end
    end
end)

minetest.register_chatcommand("shadow_mute", {
    params = "<name>",
    func = function(name, param)
        local player = minetest.get_player_by_name(param)
        if not player then
            return false, "Player is not online!"
        end
        chat_anticurse.shadow_muted[param] = true
        return true, param.." is now shadow muted!"
    end,
    privs = { shadow_mute = true }
})

minetest.register_chatcommand("shadow_unmute", {
    params = "<name>",
    func = function(name, param)
        if not chat_anticurse.shadow_muted[param] then
            return false, "Player was not shadow muted!"
        end
        chat_anticurse.shadow_muted[param] = nil
        return true, param.." is no longer shadow muted!"
    end,
    privs = { shadow_mute = true }
})

minetest.register_privilege("shadow_mute", {
    description = "Ability to shadow mute players."
})

local storage = minetest.get_mod_storage("chat_anticurse")

minetest.register_chatcommand("anticurse_add", {
    params = "<player_name> <word>",
    description = "Add a forbidden word for a specific player",
    privs = {server = true},
    func = function(name, param)
        local target, word = param:match("(%S+)%s+(.+)")
        if not target or not word then
            return false, "Usage: /anticurse_add <player_name> <word>"
        end

        local stored_words = minetest.deserialize(storage:get_string(target)) or {}
        table.insert(stored_words, word)
        storage:set_string(target, minetest.serialize(stored_words))
        return true, "Added forbidden word '" .. word .. "' for player " .. target .. "."
    end,
})

minetest.register_chatcommand("anticurse_remove", {
    params = "<player_name> <word>",
    description = "Remove a forbidden word for a specific player",
    privs = {server = true},
    func = function(name, param)
        local target, word = param:match("(%S+)%s+(.+)")
        if not target or not word then
            return false, "Usage: /anticurse_remove <player_name> <word>"
        end

        local stored_words = minetest.deserialize(storage:get_string(target)) or {}
        for i, w in ipairs(stored_words) do
            if w == word then
                table.remove(stored_words, i)
                storage:set_string(target, minetest.serialize(stored_words))
                return true, "Removed forbidden word '" .. word .. "' for player " .. target .. "."
            end
        end
        return false, "Forbidden word '" .. word .. "' not found for player " .. target .. "."
    end,
})

minetest.register_chatcommand("anticurse_list", {
    params = "<player_name>",
    description = "List forbidden words for a specific player",
    privs = {server = true},
    func = function(name, param)
        local stored_words = minetest.deserialize(storage:get_string(param)) or {}
        return true, "Forbidden words for " .. param .. ": " .. table.concat(stored_words, ", ")
    end,
})

local old_check_message = chat_anticurse.check_message
chat_anticurse.check_message = function(name, message)
    local result = old_check_message(name, message)
    if result ~= 0 then
        return result
    end
    local stored_words = minetest.deserialize(storage:get_string(name)) or {}
    for _, word in ipairs(stored_words) do
        if message:lower():find(word:lower()) then
            return 2
        end
    end

    return 0
end
